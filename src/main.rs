extern crate colored;

use colored::*;

fn main() {
    print!("{} ", "▼ black".black());
    print!("{} ", "▼ red".red());
    print!("{} ", "▼ green".green());
    print!("{} ", "▼ yellow".yellow());
    print!("{} ", "▼ blue".blue());
    print!("{} ", "▼ magenta".magenta());
    print!("{} ", "▼ cyan".cyan());
    println!("{} ", "▼ white".white());

    print!("{} ", "▼ black".black().on_black());
    print!("{} ", "▼ red".red().on_red());
    print!("{} ", "▼ green".green().on_green());
    print!("{} ", "▼ yellow".yellow().on_yellow());
    print!("{} ", "▼ blue".blue().on_blue());
    print!("{} ", "▼ magenta".magenta().on_magenta());
    print!("{} ", "▼ cyan".cyan().on_cyan());
    println!("{} ", "▼ white".white().on_white());

    println!("");

    print!("{} ", "▲ black".bright_black());
    print!("{} ", "▲ red".bright_red());
    print!("{} ", "▲ green".bright_green());
    print!("{} ", "▲ yellow".bright_yellow());
    print!("{} ", "▲ blue".bright_blue());
    print!("{} ", "▲ magenta".magenta());
    print!("{} ", "▲ cyan".bright_cyan());
    println!("{} ", "▲ white".bright_white());

    print!("{} ", "▲ black".bright_black().on_bright_black());
    print!("{} ", "▲ red".bright_red().on_bright_red());
    print!("{} ", "▲ green".bright_green().on_bright_green());
    print!("{} ", "▲ yellow".bright_yellow().on_bright_yellow());
    print!("{} ", "▲ blue".bright_blue().on_bright_blue());
    print!("{} ", "▲ magenta".bright_magenta().on_bright_magenta());
    print!("{} ", "▲ cyan".bright_cyan().on_bright_cyan());
    println!("{} ", "▲ white".bright_white().on_bright_white());

}
